from functions import m_json
from functions import m_pck

#setting paths for mesured- and metadata
path = "/home/pi/Documents/Thonny_Dokuments/calorimetry_home/datasheets/setup_newton.json"
datafolder_path = "/home/pi/Documents/Thonny_Dokuments/calorimetry_home/data/newton"
folder_path = "/home/pi/Documents/Thonny_Dokuments/calorimetry_home/datasheets"

#if needed check sensors
#check = m_pck.check_sensors()

#getting and completing metadata
metadata = m_json.get_metadata_from_setup(path)
m_json.add_temperature_sensor_serials(folder_path, metadata)

#running the mesurement
data = m_pck.get_meas_data_calorimetry(metadata)

#saving all data
m_pck.logging_calorimetry(data, metadata, datafolder_path, folder_path)
m_json.archiv_json(folder_path, path, datafolder_path)